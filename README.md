# Pavement Data Science Workshop

Lab activities for the March 21, 2022 data science workshop at the International Data Science for Pavements Symposium at the Turner Fairbanks Research Center.

# Getting Started

Workshop participants may use an online version of RStudio using the Launch Binder button below:

`~*~*~*~`
**Launch Binder:** [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fhigh-street%2Fpavement-data-science-workshop/master?urlpath=rstudio)
`~*~*~*~`

(Note: the link above may sometimes fail. Typically, refreshing and trying again will work. Launch may also fail to due [resource availability](https://mybinder.readthedocs.io/en/latest/about/status.html).)

Workshop participants wishing to work on their local workstation should:

1. Download or clone a copy of this repository (click on the three dots in the upper-right hand corner of this page and choose "Download repository")
2. Download and install R: https://cran.r-project.org/mirrors.html
3. Download and install the RStudio Desktop IDE: https://www.rstudio.com/products/rstudio/download/
4. Install the required packages by running the following commands from within RStudio:

```R
install.packages("data.table")
install.packages("ggplot2")
```

# Resources

Getting Started: [RStudio IDE Cheatsheet](https://raw.githubusercontent.com/rstudio/cheatsheets/main/rstudio-ide.pdf)

Lab 1: [Base R Cheatsheet](https://github.com/rstudio/cheatsheets/raw/main/base-r.pdf)

Lab 2: [data.table Cheatsheet](https://raw.githubusercontent.com/rstudio/cheatsheets/main/datatable.pdf)

Lab 3: [ggplot2 Cheatsheet](https://raw.githubusercontent.com/rstudio/cheatsheets/main/data-visualization.pdf)


# Labs

## Lab 1

Intro to R Studio
Basic R Syntax


## Lab 2

 * Reading, Viewing, and Joining Data in R
   * Importing data
   * Joining data
 * Exploratory Data Analysis
   * Summary Statistics
   * Histograms
   * Correlation Matrix
 * Transformations (long-to-wide and wide-to-long)


## Lab 3

Data visualization using `ggplot2`

## Lab 4

Estimating pavement deterioration using linear regression models


# About

March, 2022

Lab Exercise Authors:

 * Mark Egge (egge at highstreetconsulting dot com)
 * Ryan Loos (loos at highstreetconsulting dot com)

_Created by High Street under contract through the FHWA Data Visualization Center._

License: MIT License