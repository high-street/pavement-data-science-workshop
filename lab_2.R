# INFO #########################################################################
#
# TITLE: PAVEMENT DATA SCIENCE LAB 2 - DATA MANIPULATION
# PURPOSE: DATA MANIPULATION AND CLEANING WITH DATA.TABLE
# OBJECTIVES: 
#  1. Import data using fread
#  2. Explore datasets
#  3. Merge datasets on common keys
#  4. Filter rows by attribute values & Calculate new column values
#  5. Reshape data from long to wide and wide to long
#  6. Reorder data

# Instructions: Run each line of code below.
# Replace all instances of #___________________# with your own code
# stopifnot() functions check your work

# IMPORTING DATA ----

##
## libraries ----
##
#import necessary libraries
library(data.table)
library(ggplot2)

##
## read data ----
##
# read in UDOT sections dataset
# source: https://data-uplan.opendata.arcgis.com/datasets/historic-pavement-section-data-open-data/
sections <- fread("data/pavement_section_data.csv")

# read in UDOT attributes dataset
# source: https://maps.udot.utah.gov/arcgis/rest/services/Complex/PAVE_PaveMgmtLevelHistory/MapServer/1
# read "data/pavement_attributes.csv" into a variable named `attrs`
#___________________#
stopifnot(nrow(attrs) == 1340)


##
# JOINING DATASETS #############################################################
##
# take a minute to open each data frame. Which fields are common between the two?

# Set DT to the merged result of sections and attrs datasets on common attributes
# using `by = c("ROUTE", "DIR", "LOCATION", "REGION", "MILES")` 
# See ?merge
#___________________#
stopifnot(nrow(DT) == 1234)

##
# DATA.TABLE BASICS ############################################################
##

##
## data.table syntax ----
##

# basic data.table syntax: 
#     DT[ i,  j,  by ] # + extra arguments
#         |   |   |
#         |   |    -------> grouped by what?
#         |    -------> what to do?
#           ---> on which rows?

# for more, run ?data.table
# or see https://raw.githubusercontent.com/rstudio/cheatsheets/main/datatable.pdf

#
### filtering and aggregating rows ----
#

# select (all columns for) rows where `SURFACE_TY == "Gravel"`
DT[SURFACE_TY == "Gravel"]

# select ROUTE, LOCATION, SURFACE_TY columns for rows  where `SURFACE_TY == "Gravel"`
DT[SURFACE_TY == "Gravel", .(ROUTE, LOCATION, SURFACE_TY)]

# add the AADT_2011 column to the results above:
DT[SURFACE_TY == "Gravel", .(ROUTE, LOCATION, SURFACE_TY, AADT_2011)] #REMOVE

# create a frequency table of records by SURFACE_TY
#___________________#

# return count of records by SURFACE_TY
# .N is a "special" data.table column that returns the count of records
DT[, .N, by = SURFACE_TY]

# return `sum(MILES)` by SURFACE_TY
DT[, sum(MILES), by = SURFACE_TY] #REMOVE

# calculate mean OCI_12 by REGION
#___________________#


##
## filtering rows ----
##

# remove segments where SURFACE_TY == "Gravel"
nrow(DT[SURFACE_TY == "Gravel"])
DT <- DT[SURFACE_TY != "Gravel"]

# keep only rows in DT where the year overlaid (YR_OVL) is before 2013
#___________________#
stopifnot(nrow(DT[YR_OVL >= 2013]) == 0)

##
## calculating fields ----
##

# to derive new fields, use := in j, e.g. `DT[, new_field = old_field + 1]`
# Note: := makes a permanent change to your data table!

# create a new unique 'id' for each section by
# concatenating ROUTE, DIR, and OBJECTID, joined with a hyphen
DT[, id := paste(ROUTE, DIR, OBJECTID, sep = "-")]

# calculate `gap` as the years between YR_OVL and YR_SURF
#___________________#
hist(DT$gap)
stopifnot(sum(DT$gap) %between% c(30000, 38000))

# replace "0" distress scores with NA
DT[DT == 0] <-NA

# EXPLORATORY ANALYSIS #########################################################

##
## viewing and getting summary information ----
##

# print the first  rows of the new data.table
head(attrs)

# print the data.table structure
str(sections)

# view variable summaries
summary(DT)

# create a table counts of DT by class and region
table(DT$CLASS, DT$REGION)

# create a table of segment counts surface type (SURFACE_TY) and region (REGION)
#___________________#

# add up miles by YR_OVL (overlay year)
DT[, sum(MILES), by = YR_OVL]

# add up miles by YR_OVL (overlay year) and display results in order
DT[order(YR_OVL), sum(MILES), by = YR_OVL]

# add up miles by YR_SURF, and order results by YR_SURF
#___________________#

# view histograms of distribution of 2012 overall condition index (OCI_12),
# ride score (RIDE_12), rutting (RUT_12) and faulting (FALT_12)
hist(DT$OCI_12)
hist(DT$RIDE_12)
#___________________#
#___________________#

##
## correlation matrix ----
## 

# create a correlation matrix for 2012 distress measures
corr <- cor(DT[, .(OCI_12, RIDE_12, RUT_12, FALT_12)], use = "pairwise.complete.obs")
(corr <- round(corr, 2)) # round to two decimal places and print to console


##
# RESHAPE & REORDER ############################################################
##

# dcast() - long to wide
# melt() - wide to long

# convert from wide to long for ride score attributes
long <- melt(
  data = DT, 
  id.vars = c("id", "SURFACE_TY", "REGION", "YR_SURF", "CLASS", "MILES", "AADT_2011"),
  measure.vars = c("OCI_04", "OCI_05", "OCI_06", "OCI_07", "OCI_08",
                   "OCI_09", "OCI_10", "OCI_11", "OCI_12"),
  value.name = "OCI",
  variable.name = "obs_year"
)

# view the new data.table
# how does the new row count compare to the old row count?

# inspect new OCI field
summary(long$OCI)
hist(long$OCI)

# some OCI values are coded as NA or -1 (presumably "missing") - remove these
# pay attention to the row count in the Environment window when filtering data
# how many rows does this remove?
#___________________#
stopifnot(all(long$OCI >= 0) & nrow(long) > 10000)

# create a new numeric column called "year" with the year of the OCI observation.
# use `substr()` to extract the two digit year from the "obs_year" column value (e.g. "OCI_04" --> "04")
# then convert to an integer and add 2000.
#___________________#
stopifnot(nrow(long[year == 2005]) %between% c(1200, 1250))


# Bonus Question:
# use `long[, round(sum(MILES)), by = .(CLASS, REGION)]` to sum MILES by class and region
# use `dcast()` to transform this result from long to wide format, e.g.
#         CLASS   1   2   3    4
# 1: Interstate 163 255  63  406
# 2:    Level 1 533 387 682 1158
# 3:    Level 2 270 126 250 1257
#___________________#

##
## reordering ----
##

# using `data.table::setorder()`
# order by segment length (MILES) in from longest to shortest
#___________________#
stopifnot(first(long$MILES) > 20)

# reorder by id and year
setorder(long, id, year)

# inspect OCI by year
ggplot(long, aes(x = as.factor(year), y = OCI)) +
  geom_boxplot()

# inspect count of segments last surfaced by year
ggplot(long, aes(x = as.factor(YR_SURF))) +
  geom_bar()

##
# SAVING ----
##

# save data.table as an R object
saveRDS(long, file = "long.rds")

# write the merged data.table to a .csv file to be read in later 
# - all variables converted to numeric or character
fwrite(long, "long.csv")

# BONUS EXERCISE
# Calculate the median segment variance for RIDE and OCI distress scores
# Which distress indicator has less median variance?
median(long[, var(OCI), by = id]$V1, na.rm = TRUE)

#___________________#
