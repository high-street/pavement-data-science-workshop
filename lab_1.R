#################################### INFO ######################################
#
# AUTHOR: Ryan Loos
# DATE: 03/15/2022
# PURPOSE: INTRO TO R LAB SECTION
# OBJECTIVES: 
#  1. Tour of R Studio
#  2. Data Types
#  3. Obtaining Summary Information
#  4. Loading Packages/Reading Data
#  5. Joining Data
#
# CHEAT SHEET: https://iqss.github.io/dss-workshops/R/Rintro/base-r-cheat-sheet.pdf
################################ A QUICK INTRO #################################

#save the character string "Hello World" as a variable (object)
hw <- "Hello World"
#print the hw variable
hw
print(hw)

#what type of variable is hw?
class(hw)
#class is a function.
#Use '?' to read documentation about this built-in function
?class

#do a quick calculation with some different operators
#before running each line, guess what you think the output will be!
3^3
4.2 + 7
50 > 16
50 > sixteen #we haven't created this variable yet!
50 > "sixteen" #this coerces 50 to "50" and checks alphabetical!
"50" > "sixteen"
10 == 5 + 5
TRUE == 1 #TRUE coerces to 1, FALSE coerces to 0
5 != 4
"Hello" != "Hola"


################################# DATA TYPES ###################################
####
#### vectors ----
####
#create a vector of integers between 1 and 10 inclusive
v <- c(1,2,3,4,5,6,7,8,9,10)
v

#take the mean of the vector
mean(v)

#another method to make the same vector
v <- 1:10
v

#test the integer vector using a logical operator
v > 5 #result is a boolean vector of the same length

#what happens when we make a vector with mixed types?
v <- c("Dog", 25.5, TRUE)
v
class(v) #the numeric 25.5 and boolean TRUE were coerced to type character

#print the second item in our vector
v[2]

#sort a character vector
sort(c("1", "a", "50", "Z", "A"))

####
#### data frames ----
####
bmi <- 	data.frame(
  gender = c("Male", "Male","Female", "F"),
  height = c(162, 151.5, 175, 141), 
  weight = c(91, 83, 102, 75),
  age = c(32, 48, 26, 46)
)
bmi

#select data.frame attributes by [row, column] indexing
#specific item
bmi[2, 3]
#entire column by index
bmi[, 2]
#entire column by name reference
bmi$height 

# print the "gender" attribute values to console
#________#

#change observation where gender = "F" to gender = "Female"
#________#

stopifnot(table(bmi$gender)[[1]] == 2)

# select the height attribute and check its class
# assign the class to variable y
#___________________#

stopifnot(y == "numeric")

# use the `table()` function to create a frequency table of `bmi$gender`
?table
#________#

####
#### factors ----
####
#create a stand alone factor
factor_gender <- factor(bmi$gender)
factor_gender

#convert the gender column in the bmi data frame to factor
#__________#

stopifnot(class(bmi$gender) == "factor")

# concatenate strings
paste("Hello", "World") # default: sep = " "
paste("Hello", "World", sep = ", ") # override default
paste0("Hello", "World") # `paste0()` is the same as `paste(..., sep = "")`

####
#### coercion ----
####

as.numeric(TRUE) #logicals convert to numeric

c("a", 2, FALSE) #values coerce to simplest type required to represent all info
#in this example 2 can be "2", and FALSE can be "FALSE". "a" cannot be 2, see below

as.numeric("a") #characters cannot be numeric, but...
as.numeric("200") #a character that would actually convert to a value can!
#this can be dangerous when converting a character column of a data frame to
#numeric and should be done with care. NAs may be introduced (which can be okay)

#coercion order is roughly logical < integer < numeric < complex < character < list
#we demonstrated coercion with a basic vector, but these concepts apply to other
#data types as well. Some examples:

as.data.frame(list(2,3,4,5)) #list to data.frame
as.data.frame(matrix(c(2,2,3,3), nrow = 2)) #matrix to data.frame

############################### BONUS EXERCISES ################################
####
#### lists ----
####
#create a list like our troublesome vector, and add a vector
l <- list(6, "May", TRUE, c(1,2,3))
l #no coercion this time!

#access the 4th item of the list (which is a vector)
l[4]

#access the 2nd item of the vector
l[[4]][2]

# Set x to the 3rd item of the vector that is in the list

#______________________#

stopifnot(x == 3)

#to unlist or convert a list to a vector:
unlisted_l <- unlist(l)
unlisted_l
#two things to notice:
# 1.) we coerce to an acceptable data type
# 2.) the vector inside the list was also included as individual items

####
#### extra ----
####

#create a vector of 6 family/friend/pet names
#__________#

#create a vector of their ages
#__________#

#create a vector for how long you have known them
#__________#

#create a boolean vector set to TRUE if they are related to you, FALSE if not
#__________#

#create a character vector containing the first word that comes to mind
#when you think about this person
#_________#

#create a data frame from the five vectors created above
#_________#

#add a new column to the data frame that converts how long you've known them to months
#_________#

#using the order() function, sort the data frame by the newly generated column
#to put the person you've known longest at the top and
#save this data frame to a new variable
#_________#

#use View() to review the result in RStudio
#_________#
